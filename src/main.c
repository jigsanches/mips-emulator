#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/* 
*@author: Gabriel Sanches
*@author: Fábio Henrique
 */

//#define DEBUG
#define $n0 noinst
#define $n1 noalu
#define $n2 nosys
#define FunctionArray(f) void (*(f)[])()
#define WriteRegister(rd,rs) Register[(rd)] = (rs);Register[$zero] = 0; if(Register[$sp] < 0x3000){printf("stack overflow\n");mipsend();}


/** regs é utilizado com o intuito facilitar a leitura do código quando estamos tratando
 *  de registradores sem que haja necessidade de indexar por posição
 */
enum regs {$zero, $at, $v0, $v1, $a0, $a1, $a2, $a3, $t0, $t1, $t2, $t3, $t4, $t5, $t6, $t7,
$s0, $s1, $s2, $s3, $s4, $s5, $s6, $s7, $t8, $t9, $k0, $k1, $gp, $sp, $fp, $ra};

int32_t     Register    [32] = {0};      // Vetor de REGISTRADORES
char        memory      [4096 * 4] = {0};// Vetor de MEMÓRIA
#ifdef DEBUG
    char cache[0x1000];
#endif

int32_t     hi;                         // Registrador especial HI
uint32_t    lo;                         // Registrador especial LO
uint32_t rd,rs,rt,shamt,address;        // Variáveis decodificadas a partir da instrução
int16_t imi;                            // Imediato de 16 bits decodificado a partir da instrução
 

uint32_t instruction;                   // Inteiro 32 bits que guarda a instrução codificada
uint32_t pc = 0;                        // Program Counter


/** Esta seção contém a declaração de todos os métodos de instruções tipo R, I e J,
 *  Syscalls implementadas e também funções de _error handling_
*/
void syscall();
void noalu();
void noinst();
void nosys();
void add();
void sub();
void mult();
void divide();
void mfhi();
void mflo();
void and();
void or();
void slt();
void sll();
void srl();
void sra();
void jr();
void addi();
void addiu();
void lw();
void lh();
void lb();
void sw();
void sh();
void sb();
void lui();
void andi();
void ori();
void slti();
void beq();
void bne();
void jump();
void jal();

void syscall1();
void syscall4();
void syscall5();
void syscall8();
void syscall10();
void syscall11();
void syscall12();
void syscall34();
void syscall35();
void syscall36();

void nextInstruction();
void mipsend();


/** Os 3 vetores de ponteiros de funções para cada uma das instruções definidas no trabalho
 *  alu     -> contém as instruções de tipo R
 *  ins     -> contém as instruções de tipo I e J
 *  Syscall -> contém as Syscalls
 */
FunctionArray(alu) = {sll,$n1,srl,sra,$n1,$n1,$n1,$n1,jr,$n1,$n1,$n1,syscall,$n1,$n1,$n1,mfhi,$n1,mflo,$n1,$n1,$n1,$n1,$n1,mult,$n1,divide,$n1,$n1,$n1,$n1,$n1,add,add,sub,$n1,and,or,$n1,$n1,$n1,$n1,slt}; //function pointer array for R type instructions
            //rd, rs, rt, shamt
                                                    //  0 1  2    3   4   5      8      9    10     12   13   15                                  32  33   35        40 41   43
FunctionArray(ins) = {$n0,$n0,jump,jal,beq,bne,$n0,$n0,addi,addiu,slti,$n0,andi,ori,$n0,lui,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,$n0,lb,lh,$n0,lw,$n0,$n0,$n0,$n0,sb,sh,$n0,sw};// function pointer for I/J type instructions
            //rt, rs, imediato
            //imediato imediato
FunctionArray(Syscall) = {$n2,syscall1,$n2,$n2,syscall4,syscall5,$n2,$n2,syscall8,$n2,syscall10,syscall11,syscall12,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,$n2,syscall34,syscall35,syscall36};// function pointer for sycall interruptions


int main(int argc, char const *argv[])
{
    FILE * TextSection = NULL;
    FILE * DataSection = NULL;
    int TextSize, DataSize = 0;
    TextSection = fopen(argv[1], "r");

    if(!TextSection){
        printf("problem reading file <NULL FILE>\n");
        mipsend();
    }
    fseek(TextSection, 0L, SEEK_END);

    TextSize = ftell(TextSection);

    rewind(TextSection);

    if(TextSize > 0x1000){
        TextSize = 0x1000;
    }

    //lendo dados e guardando na seção de texto e de dados respectivamente
    fread(memory, 1, TextSize, TextSection);

    if(argc >= 3) 
    {
        DataSection = fopen(argv[2], "r");
        fseek(DataSection, 0L, SEEK_END);
        DataSize = ftell(DataSection);
        rewind(DataSection);
        if(DataSize > 0x1000){
            DataSize = 0x1000;
        }
        fread(&(memory[0x2000]), 1, DataSize, DataSection);
    }

    Register[$sp] = 0x3FFC;
    Register[$gp] = 0x1800;
    //mipsend();
    while(pc < TextSize)
        nextInstruction();
    // em *argv temos os nomes dos arquivos de texto e dados respectivamente
    // vamos ler primeiro os dados e povoar a memória de acordo
    // depois disso vamos instrução à instrução executando-as
    mipsend();
}

/*******************************************************************************
 * syscall(): função utilizada apenas para chamar a syscall corretamente do    *
 * vetor de ponteiros Syscalls                                                 *
********************************************************************************/
void syscall(){
    Syscall[Register[$v0]]();
}


/*******************************************************************************
 * mipsend(): simplesmente imprime o valor dos registradores e da memória de   *
 * 0x00000000 até 0x00003fff                                                   *
********************************************************************************/
void mipsend()
{
    printf("\n");
    for(int i = 0; i < 32; i++)
    {
        printf("$%d	0x%08x\n",i,Register[i]);
    }
    for(unsigned int i = 0; i < 0x4000; i+=16)
    {
        printf("Mem[0x0000%04x]	0x%08x	0x%08x	0x%08x	0x%08x	\n",i,*((uint32_t*)&(memory[i])), *((uint32_t*)&(memory[i+4])), *((uint32_t*)&(memory[i+8])), *((uint32_t*)&(memory[i+0xC])) );
    }
    exit(0);
}

/*******************************************************************************
 * nextInstruction(): a partir da posição do PC, pega 4 bytes da memória e de- *
 * codifica como instrução usando máscaras para pegar os bits desejados, e     *
 * então dependendo de opcode chamamos uma função de tipo R, I, J ou Syscall   *
********************************************************************************/
inline void nextInstruction(){
    #ifdef DEBUG
        for(int i = 0; i < 0x1000; i++)
        {
            cache[i] = memory[i];
        }
    #endif
    instruction = *((uint32_t*)&(memory[pc]));
    pc+=4;
    rs = (instruction & 0x03E00000) >> 21;
    rt = (instruction & 0x001F0000) >> 16;
    register uint32_t opcode = (instruction & 0xFC000000) >> 26;
    #ifdef DEBUG
        printf("pc = %x\nopcode = %d\nrs = %d $%d = %d\nrt = %d $%d = %d\nrd = %d $%d = %d\nshamt = %d\nfunct = %d\nimi = %d\naddress = %d",pc-4,opcode,rs,rs,Register[rs],rt,rt,Register[rt],(instruction & 0x0000F800) >> 11,(instruction & 0x0000F800) >> 11,Register[(instruction & 0x0000F800) >> 11],(instruction & 0x000007C0) >> 6,(instruction & 0x0000003F),instruction & 0x0000FFFF,instruction & 0x03FFFFFF);
        char c;
        while(scanf("%c",&c) && c != '\n');
    #endif
    if(opcode == 0)
    {
        /* neste caso entram funções de tipo R e também Syscalls */
        rd = (instruction & 0x0000F800) >> 11;
        shamt = (instruction & 0x000007C0) >> 6;
        alu[(instruction & 0x0000003F)](rd,rs,rt,shamt);    
    }
    else
    {
        imi = instruction & 0x0000FFFF;
        address = instruction & 0x03FFFFFF;
        ins[opcode](rt,rs,imi,address);
    }
    #ifdef DEBUG
        printf("rs = %d $%d = %d\nrt = %d $%d = %d\nrd = %d $%d = %d\n\n",rs,rs,Register[rs],rt,rt,Register[rt],(instruction & 0x0000F800) >> 11,(instruction & 0x0000F800) >> 11,Register[(instruction & 0x0000F800) >> 11]);
    #endif
    #ifdef DEBUG
        for(int i = 0; i < 0x1000; i++)
        {
            if(cache[i] != memory[i]) exit(0);
        }
    #endif
}

/*******************************************************************************
 * WriteRegister(): recebe o número de um registrador e um valor para          *
 * ser escrito neste registrador, caso seja o registrador $zero, não           *
 * fazemos nada. Caso o $sp exceda o limite, temos stack overflow e            *
 * encerramos a execução                                                       *
********************************************************************************/


/*******************************************************************************
 * noalu(), noinst() e nosys(): funções usadas quando tenta-se usar uma instru-*
 * ção não implementada, imprime mensagem de erro e finaliza execução          *
********************************************************************************/
void noalu()
{
    printf("Instruction undefined\n");
    exit(0);
}

void noinst()
{
    printf("Instruction undefined\n");
    exit(0);
}

void nosys()
{
    printf("Syscall undefined\n");
    exit(0);
}

void add()
{
    WriteRegister(rd,Register[rs]+Register[rt]);
}

void sub()
{
    WriteRegister(rd,Register[rs]-Register[rt]);
}

void mult()
{
    int64_t h = ((int64_t)Register[rs] * Register[rt]);
    lo = (uint32_t) h;
    hi = h >> 32;
}

void divide()
{
    lo = Register[rs] / Register[rt];
    hi = Register[rs] % Register[rt];
}

void mfhi()
{
    WriteRegister(rd, hi);
}

void mflo()
{
    WriteRegister(rd, lo);
}
void and()
{
    WriteRegister(rd,Register[rs] & Register[rt]);
}

void or()
{
    WriteRegister(rd,Register[rs] | Register[rt]);
}

void slt()
{
    WriteRegister(rd,(Register[rs] < Register[rt]) ? 1 : 0);
}

void sll()
{
    WriteRegister(rd,(uint32_t)Register[rt] << shamt);
}

void srl()
{
    WriteRegister(rd,(uint32_t)Register[rt] >> shamt);
}

void sra()
{
    WriteRegister(rd, (Register[rt] & 0x80000000)| (Register[rt] >> shamt));
}

void jr()
{
    pc = Register[rs];
}

void addi()
{
    WriteRegister(rt, Register[rs] + imi);
}

void addiu()
{
    WriteRegister(rt, Register[rs] + (uint16_t)imi);
}

void lw()
{
    WriteRegister(rt, *(((int32_t*)(&(memory[imi + Register[rs]])))));
}

void lh()
{
    WriteRegister(rt, *(((int16_t*)(&(memory[imi + Register[rs]])))));
}

void lb()
{
    WriteRegister(rt, (int32_t)((signed char)(memory[imi + Register[rs]])));
}

void sw()
{
    *(((int32_t*)(&(memory[imi + Register[rs]])))) = Register[rt];
    #ifdef DEBUG
        if((Register[rs] + imi) < 0x2000 || Register[rs] + imi > 0x4000) exit(0);
    #endif
}

void sh()
{
    *(((int16_t*)(&(memory[imi + Register[rs]])))) = Register[rt];
    #ifdef DEBUG
        if((Register[rs] + imi) < 0x2000 || Register[rs] + imi > 0x4000) exit(0);
    #endif
}

void sb()
{
    (memory[imi + Register[rs]]) = Register[rt];
    #ifdef DEBUG
        if((Register[rs] + imi) < 0x2000 || Register[rs] + imi > 0x4000) exit(0);
    #endif
}

void lui()
{
    WriteRegister(rt, ((uint32_t)imi) << 16);
}

void andi()
{
    WriteRegister(rt, ((uint32_t)imi) & Register[rs]);
}

void ori()
{
    WriteRegister(rt, ((uint32_t)imi) | Register[rs]);
}

void slti()
{
    WriteRegister(rt, ((int32_t)imi) > Register[rs] ? 1 : 0);
}

void beq()
{
    pc = (Register[rs] == Register[rt] ? pc + (imi * 4) : pc);
}

void bne()
{
    pc = (Register[rs] != Register[rt] ? pc + (imi * 4) : pc);
}

void jump()
{
    pc = (pc & 0xF0000000) | (address << 2);
}

void jal()
{
    WriteRegister($ra, pc);
    pc = (pc & 0xF0000000) | (address << 2);
}

void syscall1(){
    // print int
    printf("%d", Register[$a0]);
}

void syscall4(){
    // print string
    printf("%s", &(memory[Register[$a0]]));
}

void syscall5(){
    // read int
    scanf("%d", &(Register[$v0]));
}

void syscall8(){
    // read string
    fgets(&(memory[Register[$a0]]),Register[$a1],stdin);
    #ifdef DEBUG
        if((Register[$a0]) < 0x2000 || Register[$a0] > 0x4000) exit(0);
    #endif
}

void syscall10(){
    // exit
    mipsend();
}

void syscall11(){
    // print char
    putc(Register[$a0], stdin);
}

void syscall12(){
    //read char
    Register[$v0] = getchar();
}

void syscall34(){
    //print hex int
    printf("%08x",Register[$a0]);
}

void syscall35(){
    //print int as binary
    char bin[32];
    uint32_t mask = 1;
    int32_t j = 31;
    register uint32_t v = Register[$a0];
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    bin[j--] = (v & mask) + '0';
    v >>= 1;
    puts(bin);
}

void syscall36(){
    //print uint
    printf("%u",Register[$a0]);
}
