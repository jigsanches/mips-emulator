
.PHONY: all
all: src/main.c
	gcc src/main.c -o obj/main -Wall -std=c11 -g -O3

.PHONY: clean
clean:
	#removing binaries
	rm -f obj/main.o obj/main

.PHONY: copyright
copyright:
	#############################################
	#@author: Fabio Henrique Serafim Pessoa     #
	#@author: Gabriel Carvalho Sanches Rocha    #
	#@prof:Renan Albuquerque Marks		    #
	#############################################
