.text
init:	jal	 main
		addi $v0, $zero, 10		# syscall exit
		syscall

media:  addi $sp, $sp, -8       # Aloca espaço para 2 int's
        sw   $fp, 4 ($sp)       # Salva $fp (callee-save)
        sw   $s0, 0 ($sp)       # Salva $s0 (callee-save)
        addi $fp, $sp, 4        # $fp = início do stack-frame
        
        add  $s0, $a0, $a1      # $s0 = a+b
        addi $t0, $zero, 2      # $t0 = 2
        div  $s0, $t0           # (a+b) / 2
        mfhi $v0                # $v0 = quociente
        
        lw   $fp, 4 ($sp)       # Restaura $fp (callee-save)
        lw   $s0, 0 ($sp)       # Restaura $s0 (callee-save)
        addi $sp, $sp, 8        # Desaloca espaco de 2 int's
        jr $ra

main:   addi $sp, $sp, -16      # Aloca espaço para 4 int's
        sw   $fp, 12 ($sp)      # Salva $fp (callee-save)
        sw   $ra,  8 ($sp)      # Salva $ra (callee-save)
        addi $fp, $sp, 12       # $fp aponta p/ início do stack-frame

        ori  $t0, $zero, 0x12fb # Endereço na variável msg
        
        addi $a0, $zero, 1
        addi $a1, $zero, 3
        sw   $t0, 4 ($sp)       # Salva msg (caller-save)
        jal  media              # $v0 = media(1,3)
        
        addi $a0, $zero, 5
        addi $a1, $zero, 7
        sw   $v0, 0 ($sp)       # Salva $v0 de media(1,3) (caller-save)
        jal  media              # $v0 = media(5,7)
        
        #lw   $a0, 4 ($sp)       # Restaura msg
        #lw   $a1, 0 ($sp)       # Restaura valor $v0 de media(1,3)
        #add  $a2, $zero, $v0    # $v0 de media(5,7)
        #jal  printf             # printf(msg, media(1,3), media(5,7))
        
        add  $v0, $zero, $zero  # Retornar 0 na main
        
        lw   $fp, 12 ($sp)      # Restaura $fp (callee-save)
        lw   $ra, 8 ($sp)       # Restaura $ra (callee-save)
        addi $sp, $sp, 16       # Desaloca espaço para 4 int's
        jr   $ra
