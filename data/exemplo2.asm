.text
	addi	$t0, $zero, 26578	# num <- 26578
	add		$t1, $zero, $zero	# inv <- 0

L1:	slt		$t2, $zero, $t0		# $t2 <- (0<num)
	beq		$t2, $zero, L2		# (0<num) ?
	
	sll		$t2, $t1, 3			# $t2 <- inv * 8
	sll		$t3, $t1, 1			# $t3 <- inv * 2
	add		$t2, $t2, $t3		# $t3 <- inv * (8+2)
	addi	$t3, $zero, 10		# $t3 <- 10
	div		$t0, $t3
	mfhi	$t4					# t4 <- num%10
	add		$t1, $t2, $t4		# inv*10 + num%10
	mflo	$t0					# $t0 <- num/10
	
	j		L1					# goto L1

L2:	add		$a0, $t1, $zero
	addi	$v0, $zero, 1		# syscall print int
	syscall
