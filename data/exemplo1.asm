.data
numero: .word 10203040
.text
	addi $t0, $zero, 10		# numero <- 5
	add  $t1, $zero, $zero	# soma <- 0
	
do: add	 $t1, $t1, $t0		# soma <- soma + numero
	addi $t0, $t0, -1		# numero <- numero - 1
	
	slt	 $t2, $zero, $t0	# $t2 <- (0 < numero)?
	bne	 $t2, $zero, do		# $t2 != 0 goto do

	add	 $a0, $zero, $t1	# $a0 <- $t1
	addi $v0, $zero, 1		# syscall print int

nop
nop

	lw $t0, numero